import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class TVShowJoin{

    public static class TSMapper extends Mapper<Object, Text, Text, Text>{
        @Override protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            String input = value.toString();
            String tokens[] = input.split(",");
            String k = tokens[0];
            String v = tokens[1];
            v = v.substring(1, v.length());
            System.out.println(" " + k + " " + v);
            if (!tryParseInt(v) && !v.equals("ABC")) {
                return;
            }
            context.write(new Text(k), new Text(v));
        }
    }


    public static class TSReducer extends Reducer<Text, Text, Text, Text> {
        @Override protected void reduce(Text key, Iterable<Text> values, Context context)
                throws IOException, InterruptedException {

            int sum = 0;
            for(Text v : values){
                if (tryParseInt(v.toString()))
                    sum += Integer.parseInt(v.toString());
            }

            context.write(key, new Text(String.valueOf(sum)));
        }
    }

    private static boolean tryParseInt(String v) {
        try {
            Integer.parseInt(v);
            return true;
        } catch(Exception e) {
            return false;
        }
    }
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {

        Job job = Job.getInstance(new Configuration(), "TV Channel Join");

        job.setJarByClass(TVShowJoin.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);


        job.setMapperClass(TSMapper.class);
        job.setReducerClass(TSReducer.class);
        job.setCombinerClass(TSReducer.class);

        FileInputFormat.setInputPaths(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
